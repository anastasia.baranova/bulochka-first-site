class App extends React.Component {
  state = {
    comments: [],
  };

  onCommentDelete = (id) => {
    const comments = this.state.comments.filter((comment) => comment.id !== id);

    this.setState({ comments });
  };

  onCommentSend = ({ text, author = "Тайный поклонник" }) => {
    const { comments } = this.state;

    comments.push({
      id: new Date().getTime(),
      text,
      author,
    });

    this.setState({ comments });
  };

  render() {
    return (
      <div className="App">
        <div className="framework-logo">
          <img src="../images/react-logo.png" />
        </div>
        
        <Header />
        <CommentsList
          comments={this.state.comments}
          onCommentDelete={this.onCommentDelete}
        />
        <div className="ui divider"></div>
        <CommentForm onCommentSend={this.onCommentSend} />
      </div>
    );
  }
}
